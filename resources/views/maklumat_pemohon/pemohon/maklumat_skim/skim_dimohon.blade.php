<div class="row mt-2 mb-2">
    <div class="table-responsive">
        <table class="table header_uppercase table-bordered table-hovered" id="table-skim">
            <thead>
                <tr>
                    <th>Bil.</th>
                    <th>Kod Skim</th>
                    <th>Nama Skim</th>
                    <th>Tarikh Permohonan</th>
                    <th>Tarikh Luput</th>
                    <th>Pusat Temuduga</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="card-footer">
    <div class="d-flex justify-content-end align-items-center my-1 ">
        <a class="me-3 text-danger" type="button" id="reset" hidden href="#">
            <i class="fa-regular fa-pen-to-square"></i>
            Kemaskini
        </a>
    </div>
</div>